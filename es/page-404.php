<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <title>Apolo &#8211; Página no encontrada</title>
        <?php include 'views/libs/header_includes.php'; ?>
    </head>

    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <div class="body_wrap">
            <div class="page_wrap">
                <?php include 'views/section_header.php'; ?>
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">URL not found</h1>
                            <div class="breadcrumbs"></div>
                        </div>
                    </div>
                </div>
                <div class="page_content_wrap page_paddings_yes">
                    <div class="content_wrap">
                        <div class="content">
                            <article class="post_item post_item_404">
                                <div class="post_content">
                                    <h1 class="page_title">404<span>Oops!</span></h1>
                                    <h2 class="page_subtitle">We are sorry!
                                        <b>Error 404!</b>
                                        <span>This page could not be found.</span>
                                    </h2>
                                    <p class="page_description margin_bottom_null">Can't find what you need? Take a moment and do</p>
                                    <p class="page_description">a search below or start from <a href="index.html">homepage</a>.</p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <?php include 'views/footer.php'; ?>
            </div>
        </div>
        <?php include 'views/libs/footer_includes.php'; ?>
    </body>
</html>
