<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Apolo &#8211; Empresa</title>

    <?php include 'views/libs/header_includes.php'; ?>
    <!--Bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="/include/bootstrap/js/bootstrap.min.js" async></script>
    <script src="/content/js/empresa/empresa.js"></script>
    <!--Fin Bootstrap-->
</head>

<body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">

<div id="page_preloader"></div>
    <!-- Body wrap -->
    <div class="body_wrap">
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
            <?php include 'views/section_header.php'; ?>
            <!-- /Header Mobile -->
            <!-- Breadcrumbs -->
            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div class="top_panel_title_inner top_panel_inner_style_1">
                    <div class="content_wrap">
                        <h1 class="page_title">EMPRESA</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">INICIO</a>
                            <span class="breadcrumbs_delimiter"></span>
                            <span class="breadcrumbs_item current">EMPRESA</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumbs -->
            <!-- Page Content -->
            <div class="page_content_wrap page_paddings_no">
                <!-- Content -->
                <div class="content">
                    <article class="post_item post_item_single">
                        <section class="post_content">
                            <!-- Welcome to our store -->
                            <div class="empty_space height_4_8em"></div>
                            <div class="sc_section">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title " style="text-transform: none;">¿Por qué elegir Apolo?</h2>
                                        <h3 class="sc_section_title sc_item_title " style="text-transform: none; padding-top: 1.3em;">Porque todos nuestros balines apuntan a una mejora constante en calidad y precisión a un precio justo.</h2>
                                        <p style="text-align: center;">Apolo nació a fines del año 2000, en el interior de la provincia de Córdoba, un pueblo, Justiniano Posse, convirtndose en la primer empresa de balines de Argentina en estar presente en los cinco continentes.</p>
                                        <p style="text-align: center;">Ofrecemos una amplia variedad de modelos y calibres, donde todos nuestros modelos son sometidos a un estricto control de calidad, para mejorarla aún más, Apolo invierte cada año innovación tecnológica en máquinas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="empty_space height_3_9em"></div>
                            <!-- /Welcome to our store -->
                            <!-- Our team -->
                            <div class="bg_dark_style_1">
                                <div class="content_wrap">
                                    <div class="empty_space height_5em"></div>
                                    <div class="sc_team_wrap">
                                        <div class="sc_team sc_team_style_team-1">
                                            <h2 class="sc_team_title sc_item_title" style="text-transform: none;">Nuestras líneas de producción</h2>
                                            <div class="sc_team_descr sc_item_descr">
                                                <p class="margin_bottom_null"></p>
                                            </div>

                                            <div class="sc_columns columns_wrap">
                                                <div style="display: flex; justify-content: center; align-items: center;">
                                                    <div class="sc_team_item_info" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                                                        <h4 class="sc_team_item_title">
                                                            <a href="shop.php?category=AIRBOSS">Línea Air Boss</a>
                                                        </h4>
                                                        <div class="sc_team_item_description" style="text-align: center;">Balines tope de gama, la calidad de estos productos radica en la selección individual de cada uno, por ello son usados para competición y entrenamiento. Se caracterizan por sus agrupaciones e impactos precisos.</div>
                                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div>
                                                <div class="sc_team_item_info" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                                                    <h4 class="sc_team_item_title">
                                                        <a href="shop.php?category=PREMIUM">Línea Premium</a>
                                                    </h4>
                                                    <div class="sc_team_item_description" style="text-align: center;">Balines de alta calidad, correcta terminación debido a su proceso de fabricación por estampado. Dependiendo de cada modelo pueden ser usados para caza, diversión y entrenamiento.</div>
                                                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="sc_team_item_info" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                                                        <h4 class="sc_team_item_title">
                                                            <a href="shop.php?category=ESTANDAR">Línea Estandar</a>
                                                        </h4>
                                                        <div class="sc_team_item_description" style="text-align: center;">Balines económicos, usados principalmente para diversión, puntería, y formación de futuros cazadores.</div>
                                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="empty_space height_5em"></div>
                                </div>
                            </div>
                            <!-- /Our team -->



                            <!-- Book Right Now  book_now_section_bg -->
                            <div class="responsive_bg">
                                <div class="content_wrap">
                                    <!-- Countdown -->
                                    <div class="empty_space height_2_65em"></div>
                                    <h3 class="sc_title sc_title_regular margin_bottom_small" style="text-align: center;">NUESTROS NÚMEROS</h3>
                                    <div class="row w-100 mb-2 d-flex justify-content-center" style="margin-bottom: 35px !important;">
                                        <div class="col-md-3">
                                            <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                                <div class="sc_skills_item sc_skills_style_1 odd first">
                                                    <div class="sc_skills_icon icon-iconmonstr-crosshair-7-icon"></div>
                                                    <div style="    font-size: large;  font-weight: bold;  margin-top: 1em;">60.000.000</div>

                                                    <div class="sc_skills_info">
                                                        <div class="sc_skills_label">Balines por mes</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="col-md-3">
                                            <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                                <div class="sc_skills_item sc_skills_style_1 odd first">
                                                    <div class="sc_skills_icon icon-location"></div>
                                                    <div style="    font-size: large;  font-weight: bold;  margin-top: 1em;">42</div>
                                                    <div class="sc_skills_info">
                                                        <div class="sc_skills_label">Paises</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                                <div class="sc_skills_item sc_skills_style_1 odd first">
                                                    <div class="sc_skills_icon icon-calendar-light"></div>
                                                    <div style="    font-size: large;  font-weight: bold;  margin-top: 1em;" id='aniosTrayectoria'>22</div>
                                                    <div class="sc_skills_info">
                                                        <div class="sc_skills_label">Años de Trayectoria</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /Countdown -->
                                </div>
                            </div>
                            <!-- /Book Right Now -->
                        </section>
                    </article>
                </div>
            </div>
            <!-- /Page Content -->

            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->
    </div>
    <!-- /Body wrap -->

    <?php include 'views/libs/footer_includes.php'; ?>

</body>

</html>