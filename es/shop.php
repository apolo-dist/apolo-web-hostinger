<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Apolo &#8211; Productos</title>

    <?php include 'views/libs/header_includes.php'; ?>
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--Bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="../include/bootstrap/js/bootstrap.min.js" async></script>
    <!--Fin Bootstrap-->
    <link rel="stylesheet" href="/include/css/shop.css">
</head>

<body class="woocommerce woocommerce-page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_show sidebar_left">
    <div id="page_preloader"></div>
    <!-- Body wrap -->
    <div class="body_wrap bg_image">
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
            <?php include 'views/section_header.php'; ?>
            <!-- /Header -->
            <!-- Breadcrumbs -->
            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div class="top_panel_title_inner top_panel_inner_style_1">
                    <div class="content_wrap">
                        <h1 class="page_title">BALINES</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">HOME</a>
                            <span class="breadcrumbs_delimiter"></span>
                            <span class="breadcrumbs_item current">BALINES</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumbs -->
            <!-- Page Content -->
            <div class="page_content_wrap page_paddings_yes" style="    background:black">
                <div class="content_wrap">
                        <div style="background-color: rgba(128,128,128,0.1);" style="width:60%; ">
                            <div class="card-body">
                                <h5 class="card-title" style="color: whitesmoke;">Filtros</h5>
                                <div class="container" style="justify-content: center; align-items: baseline; ">
                                    <div style="margin: 0% 3% 3% 5%;">
                                        <select class="form-select" id="selectCategoria">
                                            <option selected value="0">Categorías</option>
                                            <option value="AIRBOSS">AIR BOSS</option>
                                            <option value="PREMIUM">PREMIUM</option>
                                            <option value="ESTANDAR">ESTANDAR</option>
                                            <option value="BBS">BBS</option>
                                        </select>
                                    </div>
                                    <div style="margin: 0% 3% 3% 3%;">
                                        <select class="form-select" id="selectCalibre">
                                            <option selected value="0">Calibres</option>
                                            <option value=".177 – 4.5">.177 – 4.5</option>
                                            <option value=".22 – 5.5">.22 – 5.5</option>
                                            <option value=".25 – 6.35">.25 – 6.35</option>                                           
                                            <option value=".30 - 7.62">.30 - 7.62</option>
                                            <option value=".35 - 9">.35 - 9</option>
                                            <option value=".45">.45</option>
                                            <option value=".50">.50</option>
                                        </select>
                                    </div>
                                    <div style="margin: 0% 3% 3% 3%;">
                                        <button id="botonLimpiar" type="button" class="btn btn-outline-secondary" style="background-color: gray;border-color: black!important; width: 50px;"><img src="..\content\image\refresh.svg" alt="Limpiar"  style="width:25px; height: 25px;"></button>
                                    </div>
                                </div>
                                    <div style="display: flex; margin: 0% 3% 3% 3%; justify-content: center;"> 
                                        <input style='width: 87%;' type='text' class='form-control' id='inputBuscar' aria-describedby='basic-addon2'>
                                        <button id="botonBuscar" class="btn btn-outline-secondary" type="button" style="color:#000000; width: 80px;"><img src="..\content\image\lupa.svg" alt="Buscar" style="width:25px; height: auto;"></button>
                                    </div>
                            </div>
                        </div>
                    <!-- Content -->
                    <div class="content w-100">
                        <div class="list_products shop_mode_thumbs">
                            <p id="showing_results" class="woocommerce-result-count">
                            </p>
                            <ul id="product_list_id" Full="False" cargando="False" siguiente="0" class="products row">
                            </ul>
                        </div>
                    </div>
                    <!-- /Content -->
                </div>
                <div class="empty_space height_3_9em"></div>
            </div>
            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->
    </div>
    <!-- /Body wrap -->
    <?php include 'views/libs/footer_includes.php'; ?>
    <script src="../include/js/product2.js"></script>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: black;    border-bottom-width: 0px;">
                    <h5 class="modal-title" id="tituloImagen"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center"  style="background: rgba(0,0,0,0.9);">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div id="imagenMostrar"></div>
                        </div>
                        <div class="col-md-6 col-12">
                        <div class="row">
                            <ul class="list-group w-100 mt-4 mr-4" style="margin-left: 1.5rem !important;">
                                <li class="contenedorItems">
                                    <div class="itemProducto">
                                        <h5 style="text-transform: none; padding-top: 12px;">Calibre</h5>
                                        <p id="calibreImagen" style="color: whitesmoke;"></p>
                                    </div>
                                    <div class="itemProducto">
                                        <h5 style="text-transform: none; padding-top: 12px;">Peso</h5>
                                        <p id="pesoImagen" style="color: whitesmoke;"></p>
                                    </div>
                                    <div id="cbContainer" class="itemProducto">
                                        <h5 style="text-transform: none; padding-top: 12px;">Coeficiente Balistico(BC)</h5>
                                        <p id="cbImagen" style="color: whitesmoke;"></p>
                                    </div>
                                </li>
                                <li class="list-group-item" style="background-color: black; border-radius: 6px;">
                                    <h5 style="text-transform: none;">Descripción</h5>
                                    <p id="descripcionImagen" style="color: whitesmoke;"></p>
                                </li>
                                <li class="list-group-item" style="background-color: black; border-radius: 6px;">
                                    <h5 style="text-transform: none;">Presentación</h5>
                                    <p id="presentacionImagen" style="color: whitesmoke;"></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="catalogo">
    <a class="btn" style="background-color: black; border: 1px; border-color: white; border-style: solid;display:flex;flex-direction:column;" 
    href="/content/download/CatalogoApolo2025.pdf">
        <span class="material-icons">download</span> 
        <span>Catálogo</span>
    </a>
</div>
<script>
    contenedorPrincipal = document.getElementById("product_list_id");
    $(window).scroll(
        function() {    
            var alturaTotal = $(document).height();
            var scroll = $(this).scrollTop();
            var viewport = $(this).height();
            var total = scroll+viewport+1200;
            if (total >= alturaTotal){
                var i = contenedorPrincipal.getAttribute('siguiente');
                filterProducts(i, false);
            }
        }
    );
</script>
</body>
</html>