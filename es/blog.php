<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Apolo &#8211; Novedades</title>

        <?php include 'views/libs/header_includes.php'; ?>

        <link rel="stylesheet" href="../include/css/core.portfolio.min.css" type="text/css" media="all" />
    <!--Bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="../include/bootstrap/js/bootstrap.min.js" async></script>
    <!--Fin Bootstrap-->

<style>
.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: rgba(0,0,0,0.9);
}
.card-body:hover .overlay {
  opacity: 1;
}
</style>
    </head>

    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">NOVEDADES</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.html">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">NOVEDADES</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_yes" style="background-color: black!important;">
                    <div class="content_wrap">
                    <div id="instafeed-container"></div>

                    <div class="embedsocial-hashtag" data-ref="749f32ce7741d6df8e6522090c62ea4fb54c060c" ><a class="feed-powered-by-es" href="https://embedsocial.com/social-media-aggregator/" target="_blank" title="Powered by EmbedSocial">Powered by EmbedSocial<span>→</span></a></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/cdn/ht.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialHashtagScript"));</script>


                        </div>
                        <!-- /Content -->
                    </div>
                    <!-- </div> class="content_wrap"> -->
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <?php include 'views/libs/footer_includes.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/@ptkdev/webcomponent-instagram-widget@latest/dist/lib/en/instagram-widget.min.js"></script>

    </body>

</html>
