<script src="../include/js/cartelModal.js"></script>
<style>
    .menu_main_nav li > a{
        letter-spacing: 0px !important;
    }
</style>
<!-- Menu -->
<nav class="menu_main_nav_area">
    <ul class="menu_main_nav">
        <!-- Home current-menu-ancestor current-menu-parent -->
        <li id="homeItem" class="menu-item">
            <a href="index.php">INICIO</a>
        </li>
        <!-- /Home -->
        <!-- Pages -->
        <li id="shopItem" class="menu-item menu-item-has-children">
            <a href="shop.php">PRODUCTOS</a>
            <ul class="sub-menu">
                <li class="menu-item">
                    <a href="shop.php?category=AIRBOSS">AIR BOSS</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=PREMIUM">PREMIUM</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=ESTANDAR">ESTANDAR</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=BBS">BBS</a>
                </li>
            </ul>
        </li>
        <!-- /Pages -->
        <!-- Products -->
        <li id="dealersItem" class="menu-item">
            <a href="dealers.php">DISTRIBUIDORES</a>
        </li>
        <!-- /Products -->
        <!-- Promotion -->
        <li id="aboutUsItem" class="menu-item">
            <a href="about-us.php">EMPRESA</a>
        </li>
        <!-- /Promotion -->
        <!-- Promotion -->
        <li id="certificationsItem" class="menu-item">
            <a href="certifications.php">CERTIFICACIONES</a>
        </li>
        <!-- /Promotion -->
        <!-- Blog -->
        <li id="newsItem" class="menu-item">
            <a href="blog.php">NOVEDADES</a>
        </li>
        <!-- /Blog -->
        <!-- Contact Us -->
        <li id="contactUsItem" class="menu-item">
            <a href="contact-us.php">CONTACTO</a>
        </li>
        <li id="menu-item-1969" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1969">
            <a href="https://www.facebook.com/BalinesApolo" target="_blank" rel="noopener noreferrer">
                <i class="icon-facebook" style="font-size:18px;"></i>
            </a>
        </li>
        
        <li id="menu-item-2700" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2700">
            <a href="https://www.instagram.com/balines.apolo/" target="_blank" rel="noopener noreferrer">
                <i class="icon-instagramm" style="font-size:18px;"></i>
            </a>
        </li>
        <li id="menu-item-2700" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2700">
            <a href="https://www.linkedin.com/company/apolo-argentina/" target="_blank" rel="noopener noreferrer">
                <i class="icon-linkedin" style="font-size:18px;"></i>
            </a>
        </li>
        <li id="menu-item-2700" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2700">
            <a onclick="abrirAnuncio()">
                <i class="icon-basket" style="font-size:18px;"></i>
            </a>
        </li> 
    </ul>
</nav>
<!-- /Menu -->
<div class="modal fade" id="advertenciaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow: hidden;">
  <div class="modal-dialog" role="document" style="width: 25%;margin: 17.5rem auto;">
    <div class="modal-content" style="    max-height: 100%;">
      <div class="modal-header" style="background-color: black;    border-bottom-width: 0px;">
        <h5 class="modal-title" id="tituloAdv">
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"  style="background: rgba(0,0,0,0.9);">
        <div class="row" style="margin: 5%;">
            <div style="width:100%;">
                <h4 id="advertencia" style="margin-bottom:20px;"></h4>
                <div style="display: flex; justify-content: space-evenly;">
                    <a href="https://apoloshop.com.ar/" target="_blank" rel="noopener noreferrer"><button id="ok" onclick="document.getElementById('cancel').click()" style="border-radius:4%; padding: 0.5em 2em;margin-right:10px;"></button></a>
                    <button id="cancel" type="button" data-dismiss="modal" aria-label="Close" style="border-radius:4%; padding: 0.5em 2em;"></button>
                </div>
            </div>  
        </div>
      </div>
    </div>
  </div>
</div>
<!--finModal-->