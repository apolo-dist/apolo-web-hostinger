<link rel="stylesheet" href="../include/js/vendor/woocommerce/css/woocommerce-layout.css" type="text/css" media="all" />
<link rel="stylesheet" href="../include/js/vendor/woocommerce/css/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)" />
<link rel="stylesheet" href="../include/js/vendor/woocommerce/css/woocommerce.css" type="text/css" media="all" asyn/>

<link rel="stylesheet" href="../include/js/vendor/revslider/css/settings.css" type="text/css" media="all" />
<link rel="stylesheet" href="../include/css/tpl-revslider.css" type="text/css" media="all" />

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Hind:300,400,700%7CLato:300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext">

<link rel="stylesheet" href="../include/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="../include/css/animation.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="../include/css/shortcodes-header.css" type="text/css" media="all" />

<link rel="stylesheet" href="../include/css/fontello/css/fontello.css" type="text/css" media="all" />

<link rel="stylesheet" href="../include/css/plugin.woocommerce.css" type="text/css" media="all" />

<link rel="stylesheet" href="../include/css/skin-header.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="../include/css/responsive.css" type="text/css" media="all" />

<link rel="stylesheet" href="../include/css/messages.css" type="text/css" media="all" />
<link rel="stylesheet" href="../include/js/vendor/magnific/magnific-popup.min.css" type="text/css" media="all" />
<!--<link rel="stylesheet" href="../include/js/vendor/swiper/swiper.min.css" type="text/css" media="all" />-->
<link rel="stylesheet" href="../include/css/carouselboostrap.css" type="text/css" media="all" />

<link rel="icon" href="../content/image/iconApolo.webp" sizes="32x32" />
<link rel="icon" href="../content/image/iconApolo.webp" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="../content/image/iconApolo.webp" />
