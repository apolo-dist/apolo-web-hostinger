<!-- Header -->
<header class="top_panel_wrap top_panel_style_1 scheme_original">
    <div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_above">
        <!-- Top panel 1 -->
        <div class="top_panel_middle">
            <div class="content_wrap">
                <div class="columns_wrap columns_fluid">
                    <!-- Logo -->
                    <div class="column-1_2 contact_logo" style="margin-left: 35%;">
                    <div class="logo">
                        <a href="../index.php">
                            <img src="../content/image/ApoloLogo.webp" class="logo_main" alt="" style=" width: 100; height: 320;">
                        </a>
                    </div>
                  </div>
                    <!-- /Cart -->
                <!-- Contact Phone -->
                <!--
                <div class="column-1_4 contact_field contact_phone">
                    <span class="contact_icon icon-iconmonstr-phone-2-icon"></span>
                    <span class="contact_label contact_phone">+54-3537-430 320</span>
                    <span class="contact_email">info@apolo.ar</span>
                </div>
                -->
                <!-- /Contact Phone -->
                </div>
                </div>
            </div>
        </div>
        <!-- /Top panel 1 -->
        <!-- Top panel 2 -->
        <div class="top_panel_bottom">
            <div class="content_wrap clearfix">
                <!-- Menu -->
                <?php include "navigation_menu.php"; ?>
            </div>
        </div>
        <!-- /Top panel 2 -->
    </div>
    <script type="text/javascript">const tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "https://www.trustlogo.com/"); document.write(unescape("<script src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript' %3E%3C/script%3E"));</script>
</header>
<!-- /Header -->
<?php include "navigation_mobile.php"; ?>

<script>
  var actualPage = window.location.href;
  actualPage = actualPage.split("/");
  switch(actualPage[4]) {
    case "shop.php":
    case "shop.php?lang=en":
    case "shop.php?lang=es":
    case "shop.php?category=BBS":
    case "shop.php?category=BBS&lang=en":
    case "shop.php?category=BBS&lang=es":
    case "shop.php?category=AIRBOSS":
    case "shop.php?category=AIRBOSS&lang=en":
    case "shop.php?category=AIRBOSS&lang=es":
    case "shop.php?category=PREMIUM":
    case "shop.php?category=PREMIUM&lang=en":
    case "shop.php?category=PREMIUM&lang=es":
    case "shop.php?category=ESTANDAR":
    case "shop.php?category=ESTANDAR&lang=en":
    case "shop.php?category=ESTANDAR&lang=es":
      document.getElementById("shopItem").classList.add("current-menu-parent");
      document.getElementById("shopItemMobile").classList.add("current-menu-parent");
      break;

    case "dealers.php":
    case "dealers.php?lang=en":
    case "dealers.php?lang=es":
      document.getElementById("dealersItem").classList.add("current-menu-parent");
      document.getElementById("dealersItemMobile").classList.add("current-menu-parent");
      break;

    case "about-us.php":
    case "about-us.php?lang=en":
    case "about-us.php?lang=es":
        document.getElementById("aboutUsItem").classList.add("current-menu-parent");
        document.getElementById("aboutUsItemMobile").classList.add("current-menu-parent");
      break;

    case "certifications.php":
    case "certifications.php?lang=en":
    case "certifications.php?lang=es":
        document.getElementById("certificationsItem").classList.add("current-menu-parent");
        document.getElementById("certificationsItemMobile").classList.add("current-menu-parent");
      break;

    case "blog.php":
    case "blog.php?lang=en":
    case "blog.php?lang=es":
        document.getElementById("newsItem").classList.add("current-menu-parent");
        document.getElementById("newsItemMobile").classList.add("current-menu-parent");
      break;

    case "contact-us.php":
    case "contact-us.php?lang=en":
    case "contact-us.php?lang=es":
        document.getElementById("contactUsItem").classList.add("current-menu-parent");
        document.getElementById("contactUsItemMobile").classList.add("current-menu-parent");
      break;

    default:
      document.getElementById("homeItem").classList.add("current-menu-parent");
      document.getElementById("homeItemMobile").classList.add("current-menu-parent");
      break;
  }

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121900043-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121900043-1');
</script>
