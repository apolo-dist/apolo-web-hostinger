<script src="../include/js/cartelModal2.js"></script>
<style>
    .menu_main_nav li > a{
        letter-spacing: 0px !important;
    }
</style>
<!-- Header Mobile -->
<div class="header_mobile">
    <div class="content_wrap">
        <div class="menu_button icon-menu"></div>
        <!-- Logo -->
        <div class="logo">
          <a href="https://apolo.ar/">
              <img src="../content/image/ApoloLogo.webp" class="logo_main" alt="" style=" width: 100; height: 320;">
          </a>
        </div>
        <!-- /Logo -->
    </div>
    <!-- Side wrap -->
    <div class="side_wrap" style="overflow-y: hidden;">
        <!-- Top panel -->
        <div class="panel_top">
            <!-- Menu -->
            <nav class="menu_main_nav_area">
                <ul class="menu_main_nav">
                    <!-- Home -->
                    <li id="homeItemMobile" class="menu-item" style="display: flex;">
                        <a href="index.php" style="padding-top: 35px !important; width: 80%;">INICIO</a>
                        <div class="close" style="  color: #c8c8c8 !important;
  text-shadow: none !important;"><span class="icon-cancel"></span></div>
                    </li>
                    <!-- /Home -->
                    <!-- Pages -->
                    <li id="shopItemMobile" class="menu-item menu-item-has-children">
                        <a href="shop.php">PRODUCTOS</a>
                        <ul class="sub-menu">
                        <li class="menu-item">
                    <a href="shop.php?category=AIRBOSS">AIR BOSS</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=PREMIUM">PREMIUM</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=ESTANDAR">ESTANDAR</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=BBS">BBS</a>
                </li>
                        </ul>
                    </li>
                    <!-- /Pages -->
                    <!-- Shop -->
                    <li id="dealersItemMobile" class="menu-item">
                        <a href="dealers.php">DISTRIBUIDORES</a>
                    </li>
                    <!-- /Shop -->
                    <!-- Events -->
                    <li id="aboutUsItemMobile" class="menu-item">
                        <a href="about-us.php">EMPRESA</a>
                    </li>
                    <!-- /Events -->
                    <li id="certificationsItemMobile" class="menu-item">
                        <a href="certifications.php">CERTIFICACIONES</a>
                    </li>

                    <!-- Blog -->
                    <li id="newsItemMobile" class="menu-item">
                        <a href="blog.php">NOVEDADES</a>
                    </li>
                    <!-- /Blog -->
                    <!-- Contact Us -->
                    <li id="contactUsItemMobile" class="menu-item">
                        <a href="contact-us.php">CONTACTO</a>
                    </li>
                    <li id="menu-item-2700" class="close menu-item menu-item-type-custom menu-item-object-custom menu-item-2700" style="width: 100%;"><a onclick="abrirAnuncio2()"><i class="icon-basket" style="font-size:18px;" style="width: 100%;"></i></a></li> 
                    <li id="menu-item-1969" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1969" style="display: flex;"><a href="https://www.facebook.com/BalinesApolo" style="width: 33%;"><i class="icon-facebook" style="font-size:20px;"></i></a><a href="https://www.instagram.com/balines.apolo/" style="width: 33%;"><i class="icon-instagramm" style="font-size:20px;"></i></a><a href="https://www.linkedin.com/company/apolo-argentina/" style="width: 33%;"><i class="icon-linkedin" style="font-size:20px;"></i></a></li>
                </ul>
            </nav>
            <!-- /Menu -->

        </div>
        <!-- /Top panel -->
        <!-- Middle panel -->
        <div class="panel_middle">
            <div class="contact_field contact_address">
                <span class="contact_icon icon-home"></span>
                <span class="contact_label contact_address_1">2553 - Justiniano Posse, Córdoba, Argentina.,</span>
                <span class="contact_address_2">Av. Int. Pedro Paoloni 1594 - Parque Industrial</span>
            </div>
            <div class="contact_field contact_phone">
                <span class="contact_icon icon-phone"></span>
                <span class="contact_label contact_phone">+54-3537-430320,<br/></span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="contact_label contact_phone">+54-3537-666175</span>
            </div>
            <div class="contact_field contact_email">
            <span class="contact_icon icon-mail"></span>
            <span class="contact_email">info@apolo.ar</span>
            </div>
        </div>
        <!-- /Middle panel -->
        <!-- Bottom panel -->
        <div class="panel_bottom">
            <div class="contact_socials">

            </div>
        </div>
        <!-- /Bottom panel -->
    </div>
    <!-- /Side wrap -->
    <div class="mask"></div>
</div>
<!-- /Header Mobile -->
<!-- /Menu -->
<div class="modal fade" id="advertenciaModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow: hidden;">
  <div class="modal-dialog" role="document" style="width: 80%;margin: 20.5rem auto;">
    <div class="modal-content" style="    max-height: 100%;">
      <div class="modal-header" style="background-color: black;    border-bottom-width: 0px;">
        <h5 class="modal-title" id="tituloAdv2">
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"  style="background: rgba(0,0,0,0.9);">
        <div class="row" style="margin: 5%; display: flex; justify-content: center;">
            <div>
                <h4 id="advertencia2" style="margin-bottom:20px;"></h4>
                <div style="display: flex; justify-content: space-evenly;">
                    <a href="https://apoloshop.com.ar/" target="_blank" rel="noopener noreferrer"><button id="ok2" onclick="document.getElementById('cancel2').click()" style="border-radius:4%; padding: 0.5em 1.5em;margin-right:10px;"></button></a>
                    <button id="cancel2" type="button" data-dismiss="modal" aria-label="Close" style="border-radius:4%; padding: 0.5em 1.5em;"></button>
                </div>
            </div>  
        </div>
      </div>
    </div>
  </div>
</div>
<!--finModal-->