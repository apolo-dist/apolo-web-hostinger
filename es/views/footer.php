<!-- Footer 2-->
<style>
    #cacec{
        width: 350px;
        height: 120px;
    }
</style>
<footer class="contacts_wrap scheme_original">
    <div class="contacts_wrap_inner">
        <div class="content_wrap">
            <!-- Logo -->
            <div class="logo" style="display:flex;flex-direction:column;align-items:center;">
                <img src="../content/image/ApoloLogo.webp" class="logo_footer" alt="Apolo" style="width: 50vw; height: auto;max-width:400px;">
            </div>
            <!-- /Logo -->
            <!-- Contact Adress -->
            <div class="contacts_address">
                <address class="address_right">
                    <p>Teléfono +54-3537-430 320</p>
                    <p>Móvil +54-3537-666 175</p>
                </address>
                <address class="address_left">
                    <p>Av. Int. Pedro Paoloni 1594 - Parque Industrial</p>
                    <p>2553 - Justiniano Posse, Córdoba, Argentina</p>
                </address>
            </div>
            <!-- /Contact Adress -->
            <!-- Socials -->
            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_medium">
                <div class="sc_socials_item">
                    <a href="https://www.facebook.com/BalinesApolo" target="_blank" class="social_icons social_facebook">
                        <span class="icon-facebook"></span>
                    </a>
                </div>
                
                <div class="sc_socials_item">
                    <a href="https://www.instagram.com/balines.apolo/" target="_blank" class="social_icons social_instagram">
                        <span class="icon-instagramm"></span>
                    </a>
                </div>

                <div class="sc_socials_item">
                    <a href="https://www.linkedin.com/company/apolo-argentina/" target="_blank" class="social_icons social_instagram">
                        <span class="icon-linkedin"></span>
                    </a>
                </div>
            </div>
            <!-- /Socials -->
        </div>
    </div>
</footer>
<!-- /Footer 2-->
<!-- Certifications footer -->
<footer class="footer_wrap widget_area scheme_original" style="height: 400px">
    <div class="footer_wrap_inner widget_area_inner" style="padding-bottom: 55px; padding-top: 20px; display: flex; flex-direction: column;  justify-content: center; align-items: center;">
        <div style="height: 200px; width: 30%; display: flex; flex-direction: row;  justify-content: space-around; align-items: center;">
                <img width="77" height="125" src="../content/image/logo_iram_iso_14000.webp" class="image wp-image-1647  attachment-full size-full" alt="" style="max-width: 100%; height: auto;"/>
                <img width="77" height="125" src="../content/image/logo_iram_iso_9000.webp" class="image wp-image-1646  attachment-full size-full" alt="" style="max-width: 100%; height: auto;"/>
                <img width="77" height="152" src="../content/image/logo_exporta.webp" class="image wp-image-1645  attachment-full size-full" alt="" style="max-width: 100%; height: auto;"/>
        </div>
        <div style="height: 150px; width: 80%; text-align: center !important;">
            <a href="http://www.cacec.com.ar/">
                <img id="cacec" src="../content/image/logo-cacec-300x99.webp" alt="" srcset="../content/image/logo-cacec-300x99.webp 300w, ../content/image/logo-cacec-768x254.webp 768w, ../content/image/logo-cacec-600x199.webp 600w, ../content/image/logo-cacec.webp 876w" style="width: 50vw; height: auto;max-width:300px;"/>
            </a>
        </div>    
    </div>
</footer>

<!-- /Certification footer -->
<!-- Copyright -->
<div class="copyright_wrap copyright_style_text  scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
         
            <div class="copyright_text">
                <a href="#">Copyright Apolo 2023</a>
            </div>
        </div>
    </div>
</div>
<!-- /Copyright -->
