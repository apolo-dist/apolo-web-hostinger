<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    
    
    <title>Apolo &#8211; Contactanos</title>

    <?php include 'views/libs/header_includes.php'; ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="../include/bootstrap/js/bootstrap.min.js" async></script>
    <style>
        #mapid {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        /* Set the size of the div element that contains the map */
        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }
    </style>

</head>

<body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
    <script>
        window.onload = function() {
            var mymap = L.map('mapid').setView([-32.8764534, -62.688582], 5);
            L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/dark_all/{z}/{x}/{y}.png', {
                maxZoom: 20               
            }).addTo(mymap);
            var icon = L.icon({
            iconUrl: '/content/image/ApoloLogo.webp',
            iconSize: [128, 40]       
            });
            var marker = L.marker([-32.8763036, -62.6877909], {icon: icon}).addTo(mymap);
            
            setTimeout(function(){
                mymap.flyTo([-32.8764534, -62.688582], 15);
                marker.bindPopup("<div style='text-align: center;'><b>Apolo Internacional</b><br> <a href='https://maps.google.com/maps?daddr=-32.8763036,-62.6877909&amp;ll' style='color:black;font-size:x-large'><i class='fas fa-map-marked-alt'></i></a> </div>");
            },2000);
            
        }
    </script>
    <div id="page_preloader"></div>
    <!-- Body wrap -->
  
    <div class="body_wrap" >
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
            <?php include 'views/section_header.php'; ?>
            <!-- Breadcrumbs -->

            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div class="top_panel_title_inner top_panel_inner_style_1">
                    <div class="content_wrap">
                        <h1 class="page_title">CONTACTANOS</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.html">HOME</a>
                            <span class="breadcrumbs_delimiter"></span>
                            <span class="breadcrumbs_item current">CONTACTANOS</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumbs -->
            <!-- Page Content -->
            <div class="page_content_wrap page_paddings_no">
                <div class="content">
                    <article class="post_item post_item_single">
                        <section class="post_content">
                            <!-- Google Map -->
                            <div id="mapid"></div>
                            
                            <!-- /Google Map -->
                            <!-- Contact Us Today -->
                            <div class="content_wrap">
                                <div class="empty_space height_6_75em"></div>
                                <div id="sc_form_2_wrap" class="sc_form_wrap">
                                    <div id="sc_form_2" class="sc_form sc_form_style_form_2">
                                        <h2 class="sc_form_title sc_item_title">
                                            CONTACTANOS
                                            <span class="thin"> HOY!</span>
                                        </h2>
                                        <div class="sc_form_descr sc_item_descr">

                                        </div>
                                        <div class="sc_columns columns_wrap">
                                            <div class="sc_form_address column-1_3">
                                                <div class="sc_form_address_field">
                                                    <span class="sc_form_address_label">Dirección</span>
                                                    <span class="sc_form_address_data"><a href="https://goo.gl/maps/dznGhGwUo2GYHNDL6" target="_blank" rel="noopener noreferrer">2553 - Justiniano Posse, Córdoba, Argentina., <br />Av. Int. Pedro Paoloni 1594 - Parque Industrial</a></span>
                                                </div>
                                                <div class="sc_form_address_field">
                                                    <span class="sc_form_address_label">Teléfono</span>
                                                    
                                                    <span class="sc_form_address_data"><i class="icon-phone"></i> (+54) 3537-430320</span><br />
                                                    &nbsp;<i class="fab fa-whatsapp"></i>&nbsp;&nbsp;<a href="https://wa.me/+5493537666175?text==Hola+me+comunico+a+traves+de+la+web+para:" class="whatsapp" target="_blank" rel="noopener noreferrer"><span class="sc_form_address_data">(+54) 3537-666175</span><br /></a>                                                </div>
                                                <div class="sc_form_address_field">
                                                    <span class="sc_form_address_label">E-mail</span>
                                                    <a href="mailto:info@apolo.ar?subject = Feedback&body = Message" target="_blank" rel="noopener noreferrer"><span class="sc_form_address_data">info@apolo.ar</span></a>
                                                </div>
                                            </div>
                                            <!--
                                            <div class="sc_form_fields column-2_3">
                                                <form id="sc_form_2_form" data-formtype="form_2" method="post" action="../include/sendmail.php">
                                                    <div class="sc_form_info">
                                                        <div class="sc_form_item sc_form_field label_over">
                                                            <label class="required" for="sc_form_username" style="display:block!important; text-align: left">Nombre</label>
                                                            <input id="sc_form_username" type="text" name="username" >
                                                        </div>
                                                        <div class="sc_form_item sc_form_field label_over">
                                                            <label class="required" for="sc_form_email" style="display:block!important; text-align: left">E-mail</label>
                                                            <input id="sc_form_email" type="text" name="email" >
                                                        </div>
                                                        <div class="sc_form_item sc_form_field label_over">
                                                            <label class="required" for="sc_form_subj" style="display:block!important; text-align: left">Asunto</label>
                                                            <input id="sc_form_subj" type="text" name="subject">
                                                        </div>
                                                    </div>
                                                    <div class="sc_form_item sc_form_message label_over">
                                                        <label class="required" for="sc_form_message" style="display:block!important; text-align: left">Mensaje</label>
                                                        <textarea id="sc_form_message" name="message" ></textarea>
                                                    </div>
                                                    <div class="sc_form_item sc_form_button" style="text-align: left">
                                                        <input type="submit" value="Enviar Mensaje" class="wpcf7-form-control wpcf7-submit"/>
                                                    </div>
                                                    <div class="result sc_infobox"></div>
                                                </form>
                                            </div>
                                            -->
                                        </div>
                                    </div>
                                </div>
                                <div class="empty_space height_8_7em"></div>
                            </div>
                            <!-- /Contact Us Today -->
                        </section>
                    </article>
                </div>
            </div>
            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->
    </div>
    <!-- /Body wrap -->

    <?php include 'views/libs/footer_includes.php'; ?>
  
</body>

</html>