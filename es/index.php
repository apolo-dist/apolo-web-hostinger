<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Apolo</title>

    <?php include 'views/libs/header_includes.php'; ?>
    <!--Bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="../include/bootstrap/js/bootstrap.min.js" async></script>
    <!--Fin Bootstrap-->
    <link rel="stylesheet" href="../include/css/parpadeo.css" type="text/css" media="all" /> 
    <link rel="stylesheet" href="../include/css/index.css" type="text/css" media="all" /> 
    <!--MATERIAL-->
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <script src="../include/js/modal.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--FIN MATERIAL-->
</head>

<body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">

    <script>
        var URLactual = window.location;
        if (URLactual.href.includes("balinesapolo.com")) {
            window.location = "https://apolo.ar/es";
        }
    </script>
    <a href="https://wa.me/+5493537666175?text=Hola+me+comunico+a+traves+de+la+web+para:" class="whatsapp" target="_blank" rel="noopener noreferrer"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
    <div id="page_preloader"></div>
    <!-- Body wrap -->
    <div class="body_wrap">
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <?php include 'views/header.php'; ?>
            <!-- /Header -->
            <!-- Carousel -->
            <div id="carouselExampleDark" class="carousel slide carousel-fade" data-bs-ride="carousel">
              <div class="carousel-indicators" style="margin-left: 20%;  margin-bottom: 0px;">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button id="b2" type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <!--<button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>-->
              </div>
              <div class="carousel-inner" style="min-height: 100%;">
                <div class="carousel-item active" style="margin-bottom: -25px;">
                  <img src="../content/image/inicio/resized/panel1.1.webp" srcset="../content/image/inicio/resized/panel1.3.webp 400w, ../content/image/inicio/resized/panel1.2.webp 800w, ../content/image/inicio/resized/panel1.1.webp 1600" class="d-block w-100" alt="..." style="width: 1100; height: 515;">
`               </div>
                <div id="carouselImage" class="carousel-item">
                  <img src="../content/image/inicio/resized/panel2.1.webp" srcset="../content/image/inicio/resized/panel2.3.webp 400w, ../content/image/inicio/resized/panel2.2.webp 800w, ../content/image/inicio/resized/panel2.1.webp 1600" class="d-block w-100" alt="..." style="width: 1100; height: 515;">
                </div>
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev" style="background-color: rgb(0,0,1); width: 4%;">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next" style="background-color: rgb(0,0,1); width: 4%;">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
            <!--Bestsellers-->
            <div class="page_content_wrap page_paddings_no">
                <div class="content" style="max-width: 100%;flex-direction: column;display: flex; justify-content: center; align-items: center;">
                <hr id="nuevosProductos" size="8px" style="  height: 10px;width: 90%;background-color: #121215;" />
                  <div class="row mt-4" style="max-width: 100%;">
                        <div class="col-12 text-center">
                            <h2 style="text-transform: none;">
                                Nuevos Productos
                            </h2>
                        </div>
                        <div class="row mb-3" style="max-width: 100%;">
                            <div class="col-4 text-center">
                            <img id="20" class="img-fluid" src="../content/image/new-products/BigBore2.webp" srcset="../content/image/new-products/BigBore-small.webp 400w, ../content/image/new-products/BigBore-medium.webp 800w, ../content/image/new-products/BigBore2.webp 1600w" alt="BIG BORE" onclick="abrirModal(this)" style="cursor: pointer; width:250px; height: auto;">
                            </div>
                            <div class="col-4 text-center">
                            <img id="21" class="img-fluid" src="../content/image/new-products/Puncher2.webp" srcset="../content/image/new-products/Puncher-small.webp 400w, ../content/image/new-products/Puncher-medium.webp 800w, ../content/image/new-products/Puncher2.webp 1600w" alt="PUNCHER" onclick="abrirModal(this)" style="cursor: pointer; width:250px; height: auto;">
                            </div>
                            <div class="col-4 text-center">
                                <img id="22" class="img-fluid" src="../content/image/new-products/Hunter2.webp" srcset="../content/image/new-products/Hunter-small.webp 400w, ../content/image/new-products/Hunter-medium.webp 800w, ../content/image/new-products/Hunter2.webp 1600w" alt="HUNTER" onclick="abrirModal(this)" style="cursor: pointer; width:217px; height: auto; margin-top: 4px;">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-4 text-center"><h5>Big Bore</h5></div>
                            <div class="col-4 text-center"><h5>Puncher</h5></div>
                            <div class="col-4 text-center"><h5>Hunter</h5></div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-4 text-center"><h6>.45 & .50</h6></div>
                            <div class="col-4 text-center"><h6>6.35</h6></div>
                            <div class="col-4 text-center"><h6>5.5</h6></div>
                        </div>
                  </div>
                  <hr size="8px" style="  height: 10px;width: 80%;background-color: #121215;" />
                  <div class="row mt-4" style="max-width: 100%;">
                        <div class="col-12 text-center">
                          <h2 style="text-transform: none;">
                              Bestsellers
                          </h2>
                        </div>
                        <div class="row mb-3" style="max-width: 100%;">
                          <div class="col-4 text-center">
                            <img id="8" class="img-fluid" src="../content/image/bestseller/Domed2.webp" srcset="../content/image/bestseller/Domed-small.webp 400w, ../content/image/bestseller/Domed-medium.webp 800w, ../content/image/bestseller/Domed2.webp 1600w" alt="DOMED" onclick="abrirModal(this)" style="cursor: pointer; width:250px; height: auto;">
                          </div>
                          <div class="col-4 text-center">
                            <img id="2" class="img-fluid" src="../content/image/bestseller/Barracuda2.webp" srcset="../content/image/bestseller/Barracuda-small.webp 400w, ../content/image/bestseller/Barracuda-medium.webp 800w, ../content/image/bestseller/Barracuda2.webp 1600w" alt="BARRACUDA" onclick="abrirModal(this)" style="cursor: pointer; width:250px; height: auto;">
                          </div>
                          <div class="col-4 text-center">
                            <img id="12" class="img-fluid" src="../content/image/bestseller/matchAirPistol2.webp" srcset="../content/image/bestseller/matchAirPistol-small.webp 400w, ../content/image/bestseller/matchAirPistol-medium.webp 800w, ../content/image/bestseller/matchAirPistol2.webp 1600w" alt="MATCH AIR PISTOL" onclick="abrirModal(this)" style="cursor: pointer; width:217px; height: auto; margin-top: 4px;">
                          </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-4 text-center"><h5>Domed</h5></div>
                            <div class="col-4 text-center"><h5>Barracuda</h5></div>
                            <div class="col-4 text-center"><h5>Match Air Pistol</h5></div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-4 text-center"><h6>.22</h6></div>
                            <div class="col-4 text-center"><h6>.22</h6></div>
                            <div class="col-4 text-center"><h6>.177</h6></div>
                        </div>
                    </div>
                    <hr size="8px" style="height: 10px;width: 90%;background-color: #121215;" />
                </div> 
            </div>

            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->

    </div>
    <!-- /Body wrap -->

    <a href="https://www.camospa.com" id="chileButton" title="Chile">
        <i class="demo-icon icon-megaphone-light"><b>APOLO<br/>CHILE</b></i>
    </a>
    <!-- 
      <a class="parpadeo" href="#nuevosProductos" id="newsButton" title="New Prodcuts" style="top: 120px !important;">NUEVOS<br/>PRODUCTOS</a>
    -->
    <?php include 'views/libs/footer_includes.php'; ?>

  <!--InicioModal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow: hidden;">
  <div class="modal-dialog" role="document" style="width: 70%;margin: 1.75rem auto;margin-top: 15vh;">
    <div class="modal-content" style="    max-height: 100%;">
      <div class="modal-header" style="background-color: black;    border-bottom-width: 0px;">
        <h5 class="modal-title" id="tituloImagen">
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"  style="background: rgba(0,0,0,0.9);">
        <div class="row">
          <div class="col-md-6 col-12">
            <div id="imagenMostrar" style="display: flex;justify-content: center;flex-direction: column;width: 100%;height: 100%;align-items: center;"></div>
          </div>
          <div class="col-md-6 col-12">
            <div class="row">
              <ul>
                <li class="contenedorItems">
                  <div class="itemProducto">
                      <h5 style="text-transform: none; padding-top: 12px;">Calibre</h5>
                      <p id="calibreImagen" style="color: whitesmoke;"></p>
                  </div>
                  <div class="itemProducto">
                      <h5 style="text-transform: none; padding-top: 12px;">Peso</h5>
                      <p id="pesoImagen" style="color: whitesmoke;"></p>
                  </div>
                  <div id="cbContainer" class="itemProducto">
                      <h5 style="text-transform: none; padding: 12px 12px 0px 12px;">Coeficiente Balistico(BC)</h5>
                      <p id="cbImagen" style="color: whitesmoke;"></p>
                  </div>
                  </li>
                <li class="list-group-item" style="background-color: black; border-radius: 6px;">
                  <h5 style="text-transform: none;">Descripción</h5>
                  <p id="descripcionImagen" style="color: whitesmoke;">
                  </p>
                </li>
              </ul>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>
</div>
<!--finModal-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>