<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Apolo &#8211; Distribuidores</title>

    <?php include 'views/libs/header_includes.php'; ?>
    <!--Bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="/include/bootstrap/js/bootstrap.min.js" async></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Fin Bootstrap-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
    <script src="/include/js/mapa/datamaps.world.min.js"></script>
    <script src="/include/js/mapa/datamaps-zoomto.min.js"></script>
    <script src="/content/js/dealers/dealers.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="/include/fontawesome/css/all.css">
    <script>
        var app = angular.module('myApp', []);
        app.controller('dealersCtrl', function($scope, $http) {
            var obtenerDealers = function() {
                return fetch("/content/data/distribuidores.json");
            }
            $scope.dealers = [];
            $http.get("/content/data/distribuidores.json")
                .then(function(response) {
                    $scope.dealers = response.data.map(function(d) {
                        if (d.telefono) {
                            d.telefono = d.telefono.trim();
                        }
                        if (d.email) {
                            d.email = d.email.trim();
                        }
                        if (d.direccion) {
                            d.direccion = d.direccion.trim();
                        }
                        if (d.website) {
                            d.website = d.website.trim();
                        }
                        return d;
                    });
                });
        });
    </script>
</head>
<body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide" ng-app="myApp" ng-controller="dealersCtrl">
    <div id="page_preloader"></div>
    <!-- Body wrap -->
    <div class="body_wrap">
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
            <?php include 'views/section_header.php'; ?>
            <!-- /Header Mobile -->
            <!-- Breadcrumbs -->
            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div class="top_panel_title_inner top_panel_inner_style_1">
                    <div class="content_wrap">
                        <h1 class="page_title">DISTRIBUIDORES</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">HOME</a>
                            <span class="breadcrumbs_delimiter"></span>
                            <span class="breadcrumbs_item current">DISTRIBUIDORES</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumbs -->
            <!-- Page Content -->
            <div class="page_content_wrap page_paddings_no">
                <!-- Content -->
                <div class="content">
                    <div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content" style="background-color: #1d1e23;">
                                <div class="modal-header">
                                    <div class="col-12" style="padding-left: 0px;padding-right: 0px;display:flex;justify-content: space-between;">
                                        <h3 id="listC2" style="color:white;">Lista de distribuidores</h3><button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                                    </div>
                                </div>
                                <div class="container" style="color:white">
                                    <div class="col-12" style="margin-bottom: 2em;"></div>

                                    <div ng-repeat='d in dealers' class="row" style="margin-bottom: 2em;">
                                        <div class="col-12 col-md-5">
                                            <h6 style="color:white;text-transform: capitalize;"><b>{{d.properties.title}}</b></h6>
                                        </div>
                                        <div class="col-12 col-md-7" style="padding-left: 2em;padding-right: 2em;">
                                            <table class="table table-striped" style="color: #cccccc;">
                                                <tbody>
                                                    <tr ng-if="d.direccion">
                                                        <th scope="row" style="border: none;width: 0em;"><span class="icon-location"></span></th>
                                                        <td style="border: none;text-align: left;">{{d.direccion}}</td>
                                                    </tr>
                                                    <tr ng-if="d.telefono">
                                                        <th scope="row" style="border: none;width: 0em;"><span class="icon-phone"></th>
                                                        <td style="border: none; text-align: left;">{{d.telefono}}</td>
                                                    </tr>
                                                    <tr ng-if="d.email">
                                                        <th scope="row" style="border: none;width: 0em;"><span class="icon-mail"></th>
                                                        <td style="border: none;text-align: left;">{{d.email}}</td>
                                                    </tr>
                                                    <tr ng-if="d.website">
                                                        <th scope="row" style="border: none;width: 0em;"><span class="icon-globe"></th>
                                                        <td style="border: none;text-align: left;"><a href="https://{{d.website}}">{{d.website}}</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr style="border: 0; clear:both; display:block; width: 96%; background-color:gray; height: 1.5px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <article class="post_item post_item_single">
                        <section class="post_content">
                            <!-- Welcome to our store -->
                            <div class="empty_space height_4_8em"></div>
                            <div class="body_wrap">
                                <!-- Page wrap -->
                                    <div class="content_wrap">
                                        <h2 id="listC">Lista de distribuidores</h2>
                                        <div id="mapa" style="position: relative;  height: 500px;margin-left:auto;margin-right:auto" class="col-12">
                                            <button style="width:99px;position:absolute;top:1em;left:1em;border-radius:12%;" id="botonMundo"><i style="font-size: xx-large;" class="fas fa-globe-americas"></i></button>
                                        </div>
                                    </div>
                            </div>
                            <div class="empty_space height_3_9em"></div>
                        </section>
                    </article>
                </div>
            </div>
            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->
    </div>
    <!-- /Body wrap -->
    <?php include 'views/libs/footer_includes.php'; ?>
</body>
</html>