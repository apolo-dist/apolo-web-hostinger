<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Apolo &#8211; Certificaciones</title>

    <?php include 'views/libs/header_includes.php'; ?>
    <!--Bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/include/bootstrap/css/bootstrap.min.css">
    <script src="/include/bootstrap/js/bootstrap.min.js" async></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="/include/css/imageGallery.css">
    <!--Fin Bootstrap-->
</head>

<body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
    <div id="page_preloader"></div>
    <!-- Body wrap -->
    <div class="body_wrap">
        <!-- Page wrap -->
        <div class="page_wrap">
            <!-- Header -->
            <link rel="stylesheet" href="../include/css/skin-2header.css" type="text/css" media="all" /> 
            <?php include 'views/section_header.php'; ?>
            <!-- /Header Mobile -->
            <!-- Breadcrumbs -->
            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div class="top_panel_title_inner top_panel_inner_style_1">
                    <div class="content_wrap">
                        <h1 class="page_title">CERTIFICACIONES</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">HOME</a>
                            <span class="breadcrumbs_delimiter"></span>
                            <span class="breadcrumbs_item current">CERTIFICACIONES</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumbs -->
            <!-- Page Content -->
            <div class="page_content_wrap page_paddings_no">
                <!-- Content -->
                <div class="content">
                    <article class="post_item post_item_single">
                        <section class="post_content">
                            <!-- Welcome to our store -->
                            <div class="sc_section">
                                <div class="content_wrap">
                                    <div class="empty_space height_2_65em"></div>
                                    <div class="row">
                                        <p style="font-size: 18px;text-align: center;">En Apolo sostenemos que el cuidado del medio ambiente está estrechamente vinculado al compromiso social que asumimos como empresa. Por eso, mantenemos una política de responsabilidad y preocupación respetando el medio ambiente, tratando a diario de contribuir de forma voluntaria a la mejora ambiental.</p>
                                        <br>
                                        <p style="font-size: 18px;text-align: center;">En reconocimiento de nuestro accionar, hemos recibido la certificación internacional de la norma ISO medio ambiente 14001 y la certificación de IRAM ISO 9001, siendo unos de los pocos en el rubro en obtenerla. Nos orgullece y nos motiva a seguir intentar mejorando día a día.</p>
                                    </div>
                                    <br><br>
                                    <div class="gallery">
                                        <img src="../content/image/9001.webp" alt="Certificacion 1" class="thumbnail">
                                        <img src="../content/image/14001.webp" alt="Certificacion 2" class="thumbnail">
                                    </div>
                                    <div class="overlay">
                                        <span class="close-btn">&times;</span>
                                        <img class="overlay-image" src="">
                                    </div>
                                    <script src="/include/js/imageGallery.js"></script>
                                </div>
                                <div class="empty_space height_5_7em"></div>
                            </div>
                        </section>
                    </article>
                </div>
            </div>
            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /Page wrap -->
    </div>
    <!-- /Body wrap -->
    <?php include 'views/libs/footer_includes.php'; ?>
</body>
</html>