const thumbnails = document.querySelectorAll('.thumbnail');
const overlay = document.querySelector('.overlay');
const overlayImage = document.querySelector('.overlay-image');
const closeBtn = document.querySelector('.close-btn');

thumbnails.forEach(thumbnail => {
    thumbnail.addEventListener('click', () => {
        const imgSrc = thumbnail.src;
        overlayImage.src = imgSrc;
        overlay.style.display = 'flex';
    });
});

closeBtn.addEventListener('click', () => {
    overlay.style.display = 'none';
});