function mostrarL() {
    document.getElementById('lingumania_currentlanglink').style.display = 'block';
    document.getElementById('lingumania_target').style.display = 'block';
    document.getElementById('bLanguage1').style.display = 'none';
    document.getElementById('bLanguage2').style.display = 'block';
}

function ocultarL() {
    document.getElementById('lingumania_currentlanglink').style.display = 'none';
    document.getElementById('lingumania_target').style.display= 'none';
    document.getElementById('bLanguage1').style.display = 'block';
    document.getElementById('bLanguage2').style.display = 'none';
}

var linguJSON = {
    "languages": [
        {
            "lang_name": "Español",
            "lang_code": "es",
            "url_pattern": "?"
        },
        {
            "lang_name": "English",
            "lang_code": "en",
            "url_pattern": "?"
        }
    ],
    "translated_segments": [
        {
            "source": "Ofreciendo una excelente penetración y elevado impacto.",
            "target_en": "Offering excellent penetration and high impact.",
            
        },{
            "source": 'Lista de distribuidores',
            "target_en": 'List of distributors',
            
        },  {
            "source": "Estampados y seleccionados uno por uno mediante una inspección automática con láser infrarojo, haciendo que sean los más precisos del mercado,logrando magnificas agrupaciones, ya sea en competencia como en cacería.",
            "target_en": "Stamped and selected one by one by means of an automatic infrared laser inspection, making them the most accurate on the market, achieving magnificent groupings, whether in competition or hunting.",
            
        },  {
            "source": "Estampados de alta gama en diferentes calibres, pesos y puntas.Pudiendo abarcar todas las necesidades de nuestros consumidores.",
            "target_en": "They are high-end prints in different gauges, weights and tips. Being able to cover all the needs of our consumers.",
            
        },
        {
            "source": "Nuestras Líneas de Producción",
            "target_en": "Production Lines",
            
        },
        {
            "source": "Nombre",
            "target_en": "Name",
            
        },
        {
            "source": "Paises",
            "target_en": "Countries",
            
        }, {
            "source": "Años de Trayectoria",
            "target_en": "Years of experience",
        },
        {
            "source": "Latas por Día",
            "target_en": "Products per day",
            
        },{
            "source": "Balines por mes",
            "target_en": "Pellets per month",
            
        },
        {
            "source": "Calibre",
            "target_en": "Caliber",
            
        }, {
            "source": "Bienvenidos",
            "target_en": "Welcome",
            
        },
        {
            "source": "Peso",
            "target_en": "Weight",
            
        }, {
            "source": "Descripción",
            "target_en": "Description",
            
        },{
            "source": "Presentación",
            "target_en": "Presentation",
            
        },
    
        {
            "source": "Calibres",
            "target_en": "Calibers",
            
        },
        {
            "source": "Catálogo",
            "target_en": "Catalogue",
            
        },
        {
            "source": "Buscar",
            "target_en": "Search",
            
        },
        {
            "source": "Limpiar Filtros",
            "target_en": "Reset",
            
        },
        {
            "source": "Categorías",
            "target_en": "Categories",
            
        },
        {
            "source": "Filtros",
            "target_en": "Filters",
        },
        {
            "source": "INICIO",
            "target_en": "HOME",
        },
        {
            "source": "BALINES",
            "target_en": "PELLETS",
        },
        {
            "source": "EMPRESA",
            "target_en": "COMPANY",
            "target_pt": "Pellets"
        },
        {
            "source": "Empresa",
            "target_en": "Company",
        },
        {
            "source": "CERTIFICACIONES",
            "target_en": "CERTIFICATIONS",
        },
        {
            "source": "DISTRIBUIDORES",
            "target_en": "LIST OF DISTRIBUTORS",
        },
        {
            "source": "NOVEDADES",
            "target_en": "NEWS",
        },
        {
            "source": "CONTACTO",
            "target_en": "CONTACT US",
        },
        {
            "source": "COMPRA AHORA!",
            "target_en": "BUY NOW!",
        },
        {
            "source": "Visitá nuestra tienda on-line",
            "target_en": "Visit our on-line store",
        },
        {
            "source": "CATALOGO BALINES",
            "target_en": "DOWNLOAD AVAILABLE PELLETS",
        },
        {
            "source": "CATALOGO EXPLORER",
            "target_en": "DOWNLOAD EXPLORER GROUP CATALOG",
        },
        {
            "source": "CATALOGO RIFLES",
            "target_en": "DOWNLOAD ARMS AND ACCESORIES CATALOG",
        },
        {
            "source": 'PRODUCTOS',
            "target_en": 'PRODUCTS ',
        },
        {
            "source": 'DESTACADOS',
            "target_en": 'PRODUCTS ',
        },
        {
            "source": 'ENCUENTRA LAS MEJORES OPCIONES EN BALINES',
            "target_en": 'FIND THE BEST OPTIONS IN PELLETS',
        },
        {
            "source": '<b>CERTIFICACIÓN NORMAS IRAM</b>',
            "target_en": 'IRAM STANDARD CERTIFICATION',
        },
        {
            "source": 'ISO DE CALIDAD 9001',
            "target_en": 'ISO QUALITY 9001',
        },
        {
            "source": 'MEDIO AMBIENTE 14001',
            "target_en": 'ENVIRONMENT 14001',
        },
        {
            "source": 'MÁS INFORMACIÓN',
            "target_en": 'MORE INFORMATION',
        },
        {
            "source": 'VER MÁS',
            "target_en": 'VIEW MORE',
        },
        {
            "source": 'CATEGORÍAS',
            "target_en": 'CATEGORIES',
        },
        {
            "source": 'NUEVO!',
            "target_en": 'NEW!',
        },
        {
            "source": 'BALINES AIR BOSS',
            "target_en": 'AIR BOSS PELLETS',
        },
        {
            "source": 'BALINES PREMIUM',
            "target_en": 'PREMIUM PELLETS',
        },
        {
            "source": 'BALINES ESTANDAR',
            "target_en": 'STANDARD PELLETS',
        },
        {
            "source": 'BALINES POR CALIBRE',
            "target_en": 'PELLETS BY CALIBER',
        },
        {
            "source": 'ENCONTRA NUESTROS PRODUCTOS EN LOS SIGUIENTES PUNTOS DE VENTA Y PAÍSES:',
            "target_en": 'FIND OUR PRODUCTS AT THE FOLLOWING POINTS OF SALE AND COUNTRIES:',
        },
        {
            "source": 'AMÉRICA',
            "target_en": 'AMERICA',
        },
        {
            "source": 'EUROPA',
            "target_en": 'EUROPE',
        },
        {
            "source": 'OCEANÍA',
            "target_en": 'OCEANIA',
        },
        {
            "source": 'BIENVENIDO',
            "target_en": 'WELCOME',
        },
        {
            "source": 'A BALINES APOLO',
            "target_en": 'TO BALINES APOLO',
        },
        {
            "source": 'Fabrica De balines Apolo es una empresa que desde el año 2001 se encuentra en constante Evolución e Innovación… siendo una de las pocas en el rubro que ha logrado Certificaciones de calidad y medio Ambiente a nivel Internacional.( 14001 y 9001).',
            "target_en": 'Apolo Air guns Factory, is a company that since 2001 is in constant evolution and innovation …. Being one of the few in the field that has achieved Quality Certifications and Environment at International level. (14001 and 9001)',
        },
        {
            "source": 'En la actualidad, se fabrican 3 Líneas de Balines de Plomo para Armas de  Aire Comprimidos y 1 línea de fabricación de balines de Acero cobreados (BBs).',
            "target_en": 'At present, 3 Lines of Air Guns Pellets for Compressed Air Weapons and 1 line of manufacture of Copper Steel Balines (BBs) are manufactured.',
        },
        {
            "source": 'Además, contamos con sistema de baño de Cobre especial, dado que el mismo no se oxida y con el tiempo sigue teniendo el mismo brillo y excelente presentación en los balines, con este recubrimiento hemos logrado en algunos modelos de balines hasta un 20% mas de Velocidad.',
            "target_en": 'In addition, we have a special copper bath system, since it does not oxidize and over time still has the same brightness and excellent presentation in the air guns pellets, with this coating we have achieved in some models of pellets up to 20% more than Speed.',
        },
        {
            "source": 'NUESTRAS',
            "target_en": 'OUR',
        },
        {
            "source": 'LÍNEAS DE PRDOUCCIÓN',
            "target_en": 'PRODUCTION LINES',
        },
        {
            "source": 'Línea Estandar o Conic',
            "target_en": 'Standard or Conic line',
        },
        {
            "source": 'Estos balines tienen una excelente relación calidad-precio para el divertimento o plinking.',
            "target_en": 'Whose pellets have an excellent value for money for diverting or plinking',
        },
        {
            "source": 'Línea Premium',
            "target_en": 'Premium Line',
        },{
            "source": 'Línea Estandar',
            "target_en": 'Standar Line',
        },
        {
            "source": 'La segunda Línea denominada Premium, son balines estampados de Alta Gama, de diferentes calibres, pesos y puntas. Pudiendo abarcar casi todo el abanico de posibilidad que un amante a la caza o al tiro al Blanco puede necesitar.',
            "target_en": 'The second line name Premium, are high-end printed pellets, of different calibers, weights and tips. Which can cover almost the whole range of possibilities that a lover hunting or target shooting may need.',
        },
        {
            "source": 'Línea Air Boss',
            "target_en": 'Air Boss Line',
        }, {
            "source": 'Línea Acero Cobreado',
            "target_en": 'Copper Steel Line',
        },
        {
            "source": 'Y nuestra ultima Línea, denomina AIR BOSS, si bien también son estampados, los mismos son seleccionados por una Línea de inspección automática con Laser infrarojo, teniendo dicha medición una tolerancia de 0.004mm. por lo que hace que sea unos de los balines más Precisos del mercado, teniendo con ellos excelentes agrupaciones tanto en Competencia como en cacería con los nuevos Rifles de PCP.',
            "target_en": 'Our last line, name AIR BOSS, although they are also printed air guns pellets, they are selected by an automatic inspection line with red infrared Laser, this measurement having a tolerance of 0.004mm. so it makes it one of the Air Guns Pellets More accurate market, having excellent groupings in both Competition and Hunting with the new PCP Rifles.',
        },
        {
            "source": 'NUESTROS NÚMEROS',
            "target_en": 'OUR NUMBERS',
        },
        {
            "source": 'BALINES POR MES',
            "target_en": 'PELLETS BY MONTH',
        },
        {
            "source": 'LATAS POR DÍA',
            "target_en": 'TINS PER DAY',
        },
        {
            "source": 'Paises',
            "target_en": 'Countries',
        },
        {
            "source": 'Años de Trayectoria',
            "target_en": 'Years of Trajectory',
        },
        {
            "source": 'H',
            "target_en": 'T',
        },
        {
            "source": 'oy en Apolo disponemos de un sistema de gestión de calidad, cumplimos con las exigencias comerciales y sociales; nos proponemos objetivos y metas ambientales.',
            "target_en": 'oday in Apolo we have a quality management system, we comply with commercial and social demands; we propose environmental objectives and goals.',
        },
        {
            "source": 'Durante muchos años, en Apolo, trabajamos para lograr la máxima calidad en nuestra fabrica y de esa manera obtener excelencia en nuestros productos.',
            "target_en": 'For many years, in Apolo, we work to achieve the highest quality in our factory and in this way obtain excellence in our products.',
        },
        {
            "source": 'Sostenemos que el cuidado del medio ambiente esta estrechamente vinculado a las metas que tienen que ver con el crecimiento',
            "target_en": 'We hold that the care of the environment is closely linked to the goals that have to do with the growth.',
        },
        {
            "source": 'Mantenemos una política de responsabilidad y preocupación respetando el medio ambiente y tratando a diario de contribuir de forma voluntaria a la mejora ambiental y con ello, nuestra competitividad en el mercado.',
            "target_en": 'We maintain a policy of responsibility and concern respecting the environment and daily trying to contribute voluntarily to environmental improvement and with it, our competitiveness in the market.',
        },
        {
            "source": 'En reconocimiento de nuestro accionar, hemos recibido la CERTIFICACIÓN INTERNACIONAL DE LA NORMA ISO MEDIO AMBIENTE 14001 Y LA CERTIFICACIÓN DE IRAM ISO 9001: 2015.',
            "target_en": 'In recognition of our actions, we have received the INTERNATIONAL CERTIFICATION OF ISO ENVIRONMENT STANDARD 14001 AND THE CERTIFICATION OF IRAM ISO 9001: 2015.',
        },
        {
            "source": 'CONTACTANOS',
            "target_en": 'CONTACT US',
        },
        {
            "source": 'HOY!',
            "target_en": 'TODAY!',
        },
        {
            "source": 'SU CORREO ELECTRONICO NO SERA PUBLICADO.',
            "target_en": 'YOUR EMAIL ADDRESS WILL NOT BE PUBLISHED.',
        },
        {
            "source": 'LOS CAMPOS REQUERIDOS SON MARCADOS CON *',
            "target_en": 'REQUIRED FIELDS ARE MARKED WITH *',
        },
        {
            "source": 'Dirección',
            "target_en": 'Address',
        },
        {
            "source": 'Teléfono',
            "target_en": 'Phone',
        },
        {
            "source": 'Móvil',
            "target_en": 'Mobile',
        },
        {
            "source": 'Nombre',
            "target_en": 'Name',
        },
        {
            "source": 'Asunto',
            "target_en": 'Subject',
        },
        {
            "source": 'Mensaje',
            "target_en": 'Message',
        },
        {
            "source": 'Enviar Mensaje',
            "target_en": 'Send Message',
        },
        {
            "source": 'ESPECIFICACIONES',
            "target_en": 'SPECIFICATIONS',
        },
        {
            "source": '<b>Calibre:</b>',
            "target_en": '<b>Caliber:</b>',
        },
        {
            "source": '<b>Peso:</b>',
            "target_en": '<b>Weight:</b>',
        },
        {
            "source": 'Categorias:',
            "target_en": 'Categories:',
        },
        {
            "source": 'ID de Producto:',
            "target_en": 'Product ID:',
        },
        {
            "source": 'IDIOMA',
            "target_en": 'LANGUAGE',
        },
        {
            "source": 'OCULTAR',
            "target_en": 'HIDE',
        },
        {
            "source": 'Teléfono +54-3537-430 320',
            "target_en": 'Phone +54-3537-430 320',
        },
        {
            "source": 'Móvil +54-3537-666 175',
            "target_en": 'Mobile +54-3537-666 175',
        },
        {
            "source": '¿Por qué elegir Apolo?',
            "target_en": 'Why choose Apolo?',
        },
        {
            "source": 'Nuevos Productos',
            "target_en": 'New products',
        },
        {
            "source": 'NUEVOS',
            "target_en": 'NEW',
        },
        {
            "source": 'Porque todos nuestros balines apuntan a una mejora constante en calidad y precisión a un precio justo.',
            "target_en": 'Because all our pellets point to a constant improvement in quality and precision at a fair price.',
        },
        {
            "source": 'Apolo nació a fines del año 2000, en el interior de la provincia de Córdoba, un pueblo, Justiniano Posse, convirtndose en la primer empresa de balines de Argentina en estar presente en los cinco continentes.',
            "target_en": 'Apolo was born at the end of the year 2000, in the interior of the province of Córdoba, a town, Justiniano Posse, becoming the first Balinese company in Argentina to be present on the five continents.',
        },
        {
            "source": 'Ofrecemos una amplia variedad de modelos y calibres, donde todos nuestros modelos son sometidos a un estricto control de calidad, para mejorarla aún más, Apolo invierte cada año innovación tecnológica en máquinas.',
            "target_en": 'We offer a wide variety of models and calibers, where all our models are subjected to strict quality control, to improve it even more, Apolo invests every year in technological innovation in machines.',
        },
        {
            "source": 'Nuestras líneas de producción',
            "target_en": 'Our production lines',
        },
        {
            "source": 'Balines tope de gama, la calidad de estos productos radica en la selección individual de cada uno, por ello son usados para competición y entrenamiento. Se caracterizan por sus agrupaciones e impactos precisos.',
            "target_en": 'Highest quality pellets, the quality of these products lies in the individual selection of each one, which is why they are used for competition and training. They are characterized by their precise groupings and impacts.',
        },
        {
            "source": 'Balines de alta calidad, correcta terminación debido a su proceso de fabricación por estampado. Dependiendo de cada modelo pueden ser usados para caza, diversión y entrenamiento.',
            "target_en": 'High quality pellets, correct finish due to its stamping manufacturing process. Depending on each model, they can be used for hunting, fun and training.',
        },
        {
            "source": 'Balines económicos, usados principalmente para diversión, puntería, y formación de futuros cazadores.',
            "target_en": 'Inexpensive pellets, used mainly for fun, aiming, and training of future hunters.',
        },
        {
            "source": 'En Apolo sostenemos que el cuidado del medio ambiente está estrechamente vinculado al compromiso social que asumimos como empresa. Por eso, mantenemos una política de responsabilidad y preocupación respetando el medio ambiente, tratando a diario de contribuir de forma voluntaria a la mejora ambiental.',
            "target_en": 'At Apolo we maintain that caring for the environment is closely linked to the social commitment that we assume as a company. For this reason, we maintain a policy of responsibility and concern respecting the environment, trying daily to contribute voluntarily to environmental improvement.',
        },
        {
            "source": 'En reconocimiento de nuestro accionar, hemos recibido la certificación internacional de la norma ISO medio ambiente 14001 y la certificación de IRAM ISO 9001, siendo unos de los pocos en el rubro en obtenerla. Nos orgullece y nos motiva a seguir intentar mejorando día a día.',
            "target_en": 'In recognition of our actions, we have received the international certification of the ISO 14001 environment standard and the IRAM ISO 9001 certification, being one of the few in the field to obtain it. It makes us proud and motivates us to keep trying to improve day by day.',
        },
        {
            "source": 'https://wa.me/+5493537666175?text=Hola+me+comunico+a+traves+de+la+web+para:',
            "target_en": 'https://wa.me/+5493537666175?text=Hello,+I+am+contacting+you+through+the+web+to:',
        }
    ]
};

