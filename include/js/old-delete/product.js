var xmlhttp = new XMLHttpRequest();

var descripciones=[];
var calibres=[];
var imprimir=[];
var url = "../content/data/productosNuevos.json";
var productArray = [];
var productItem = "";
var selectedCategoryArray = [];
var showingProducts = 0;
var lang = getUrlVars()["lang"]; 
var totalProducts = 0
var cat;
var calibre=null;
var selectedCategory=null;
var palabra=null;

if (lang == null) {
  lang = "es";
}


function cargarNuevoJson(){
  for (let index = 0; index < descripciones.length; index++) {
    const element = descripciones[index];
    if(element.id){
      let el=productArray.find(a=>(a.id==element.id));
      el.descripcion=element.Descripcion;
      el.presentacion=element.Presentacion;
      el.calibre=element.Calibre;
      el.peso=element.peso;
      imprimir.push(el);
      if(!calibres.find(a=>a==el.calibre)){
        calibres.push(e)
      }
    }
  }
  localStorage.setItem("imprimir",JSON.stringify(imprimir));
}



xmlhttp.open("GET", url, true);
xmlhttp.send();

window.onload=function(){
  var inputBuscar=document.getElementById("inputBuscar");
  var botonBuscar=document.getElementById("botonBuscar");
  var botonLimpiar=document.getElementById("botonLimpiar");
  var selectCategoria=document.getElementById("selectCategoria");
  var selectCalibre=document.getElementById("selectCalibre");
  botonLimpiar.addEventListener("click", function(){
    inputBuscar.value="";
    palabra=null;
    selectedCategory=null;
    selectCategoria.value="0";
    selectCalibre.value="0";
    calibre=null;
    filterProducts();
  });
  botonBuscar.addEventListener("click", function(){
    if(inputBuscar.value){
      if(inputBuscar.value.length>0){
        palabra=inputBuscar.value;
        filterProducts();
      }
    }
  });
 
  selectCalibre.addEventListener('change', (event) => {
    if(event.target.value=="0"){
      calibre=null;
    }else{
      calibre=event.target.value;
    }
    filterProducts();
});
  selectCategoria.addEventListener('change', (event) => {
    if(event.target.value=="0"){
      selectedCategory=null;
    }else{
      selectedCategory=event.target.value;
    }
    
    filterProducts();
});
}
/*
 * This method charge and list all the products.
 */




xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        productArray = JSON.parse(this.responseText);
        console.log(productArray)
        populateProducts();
    }
};

/*
* This methid check if an url category comes and sets the filter.
*/
document.onreadystatechange = function() {
  if (this.readyState == "complete") {
    var category = getUrlVars()["category"];
    if (category != undefined) {
      selectedCategory=category;      
     document.getElementById("selectCategoria").value=selectedCategory;    
      filterProducts();
    }
  }
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

function populateProducts() {  
  
  cargarNuevoJson();


  totalProducts = productArray.length;
  document.getElementById("product_list_id").innerHTML = "";
  productItem = "";
  for (var i = 0; i < productArray.length; i++) {
    addCurrentProduct(productArray[i]);    
  }
  document.getElementById("product_list_id").innerHTML = productItem;
}

function filterProducts() {
  
  showingProducts = 0;
  document.getElementById("product_list_id").innerHTML = "";
  productItem = "";

  
  
  //TODO: Improove this using filter functions.
  for (var i = 0; i < productArray.length; i++) {     
    var cond1=true;
    var cond2=true;
    var cond3=true;
  
    console.log()
      if(calibre){      
        calibre=calibre.replace(/\s+/g, '').toLowerCase().trim();       
        cond1=(calibre==(productArray[i].calibre.replace(/\s+/g, '').toLowerCase().trim()));
      }
      if(selectedCategory){
        cat=selectedCategory.replace(/\s+/g, '').toLowerCase().trim();
        var catProducto=productArray[i].categoria.replace(/\s+/g, '').toLowerCase().trim();
        cond2=catProducto == cat;
      }
      if(palabra){
        palabra=palabra.replace(/\s+/g, '').toLowerCase().trim();
        var buscar=productArray[i].nombre+productArray[i].categoria+productArray[i].categoria+productArray[i].descripcion+productArray[i].presentacion;
        var nombre=buscar.replace(/\s+/g, '').toLowerCase().trim();
        cond3=nombre.includes(palabra);
      }
      if(cond1 && cond2 && cond3) {
         console.log("entro")
        addCurrentProduct(productArray[i]);    
        showingProducts++;
      
    }
  }
  //document.getElementById("product_list_id").innerHTML = productItem;
  
//updateSelectedCategoryIndex(selectedCategory);
  //updateShowingResultsSection();
}

function openProductDetail(productId) {
  window.open("../es/product-detail.php?id='" + productId + "'","mywindow");
}

window.onhashchange = function() {
    var actualPage = window.location.href;
    actualPage = actualPage.split("#");
    //alert(actualPage[1]);
}

function addCurrentProduct(currentProduct) {
  //var productTranslatedName = currentProduct.name[lang];
 
  //var productName = productTranslatedName + "<br/>(" + currentProduct.caliber + ")";
  productItem+='<div class="col-12 col-md-4 col-lg-3"  id="producto'+currentProduct.id+'" >'+
  ' <div class=" wrapper"><div class="card inner" style="">'+
      ''+
      '<div class="card-body"><img id="'+currentProduct.id+'" src="'+currentProduct.nombreArchivo+'"  class="img-fluid" alt="..." onclick="abrirModal(this)" style="cursor: pointer;"> '+        
     ' </div>'+
  '</div>'+
'</div></div>';

document.getElementById("product_list_id").innerHTML = productItem;



  /*
  productItem += '<!-- Product Item --><li class="product column-1_3 "><div class="post_item_wrap"><div class="post_featured"><div class="post_thumb">';
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '">';
  if (currentProduct.new == "true") {
    productItem += '<span class="onsale">NUEVO!</span>';
  }
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '"><img src="' + currentProduct.image_small + '" alt="" title="Product" /></a>';
  productItem += '</a></div></div><div class="post_content"><h2 class="woocommerce-loop-product__title"><a id="product_name_id1" href="../es/product-detail.php?id=' + currentProduct.id + '">' + productName + '</a></h2>';
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '" class="button add_to_cart_button">VER MÁS</a></div></div></li><!-- /Product Item -->';
  */
}

function updateSelectedCategoryIndex(selectedCategory) {
  selectedCategoryArray.push(selectedCategory)
  for (let i = 0; i<selectedCategoryArray.length; i++) {
    var actualElement = document.getElementById(selectedCategoryArray[i]);
    if (actualElement.classList.contains("current-category")) {
      actualElement.classList.remove("current-category");
    }
  }
  document.getElementById(selectedCategory).classList.add("current-category");
}

function updateShowingResultsSection() {
   // document.getElementById("showing_results").innerHTML = "Mostrando " + showingProducts + " de " + totalProducts + " resultados!";
}

function abrirModal(imagen){
 console.log(imagen.id);
  console.log(imagen.src);
  console.log(lang)
 let product=productArray.find(a=>a.id==imagen.id);
 console.log(product)
 document.getElementById("calibreImagen").innerHTML=product.calibre;
 document.getElementById("pesoImagen").innerHTML=product.peso;
 if(lang=="en"){
  document.getElementById("descripcionImagen").innerHTML=product.descripcionENG;
  try {
    document.getElementById("presentacionImagen").innerHTML=product.presentacionENG;
  } catch (error) {
    console.log(error)
  }
 }else{
  document.getElementById("descripcionImagen").innerHTML=product.descripcion;
  try {
    document.getElementById("presentacionImagen").innerHTML=product.presentacion;
  } catch (error) {
    console.log(error)
  }
 }
  document.getElementById("tituloImagen").innerHTML=product.categoria;
  document.getElementById("imagenMostrar").innerHTML='<img src="'+imagen.src+'" style="max-height:500px;" alt="">';
  $("#exampleModal").modal('show');
}




