function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}
var lang = getUrlVars()["lang"];
var productArray = [
  {calibre: ".22 - 5.5"
  ,categoria: "AIR BOSS"
  ,descripcion: "Balines de Competición y entrenamiento, se caracterizan por muy buenas agrupaciones e impacto limpios y precisos. La calidad de este producto radica en la selección individual de cada balín.  Su contenedor es una lata con una lámina protectora en su interior.  "
  ,descripcionENG: "Competition and training pellets are characterized by very good groupings and clean and precise impact. The quality of this product lies in the individual selection of each pellet. Its container is a can with a protective foil inside"
  ,id: "8"
  ,nombre: "Domed"
  ,peso: "1.15 gr - 18 grains"
  ,cb: "0,028"
  },
  {calibre: ".177 - 4.5"
   ,categoria: "AIR BOSS"
   ,descripcion: "Balín cobreado pesado, utilizado para caza, de precisión excepcional para distancias largas. Sin contacto del plomo gracias al recubrimiento especial de cobre,dureza 20 % superior frente a balines de plomo. Diseño aerodinámico magnífico para un alto poder de perforación y profundidad. Baja deformación del balín. Agrupaciones pequeñas. La calidad de este producto radica en la selección individual de cada balín.  Su contenedor es una lata con una lámina protectora en su interior."
   ,descripcionENG: "Competition and training coppered pellets are characterized by very good clean and precise groupings and impact."
   ,id: "2"
   ,nombre: "Barracuda"
   ,peso: "0.7 gr - 11 grains - 25 j "
   ,cb: "0,032"
  },
  {calibre: ".177 - 4.5"
  ,categoria: "AIR BOSS"
  ,descripcion: "Especial para pistolas de aire. Balines de competicion y entrenamiento, está diseñado para ser utilizado para tiro al blanco a 10 metros. Por eso, están equipados con una cabeza plana, de modo que pueden realizar disparos lo más precisos posible. La calidad de este producto radica en la selección individual de cada balín.\nSu contenedor es una lata con una lámina protectora en su interior."
  ,descripcionENG: " Special for air pistols. Competition and training pellets, it is designed to be used for target shooting at 10 meters. That is why they are equipped with a flat head, so that they can make shots as accurate as possible. The quality of this product lies in the individual selection of each pellet.\nIts container is a can with a protective foil inside."
  ,id: "12"
  ,nombre: "Match Air Pistol"
  ,peso: "0.45 gr – 7 grains"
  ,cb: "0"
  },
  {calibre: ".45 & .50"
  ,categoria: "AIR BOSS"
  ,descripcion: "Línea tope de gama, la calidad de estos productos radica en la produccion individual de cada balín.​<br/>Debido a su forma especialmente diseñada, la distribución perfecta del centro de gravedad y la alineación, los productos big bore son la mejor opción para disparos de media y larga distancia. Los más adecuados para los rifles más potentes del mercado de aire comprimido. Absolutamente preciso y eficaz."
  ,descripcionENG: "Top of the range line, the quality of these products lies in the individual production of each pellet.​<br/>Due to its specially designed shape, the perfect distribution of the center of gravity and the alignment, the big bore products are the best option for medium and long distance shots. The most suitable for the most powerful rifles on the compressed air market. Absolutely precise and effective."
  ,id: "20"
  ,nombre: "Big Bore"
  ,peso: ".45 11.7gr - 180 grains<br/>.45 19.1 gr - 295 grains<br/>.50 33.7 gr - 520 grains<br/>.45 34 - 525 grains<br/>.50 35.6 gr – 550 grains"
  ,cb: "0"
  },
  {calibre: ".25 - 6.35"
  ,categoria: "PREMIUM"
  ,descripcion: "Balin de alta calidad, correcta terminación, debido a su proceso de fabricación por estampado. Dependiendo de cada modelo pueden ser usados para caza, diversión y entrenamiento.<br/>Balín de caza pesado, extraordinariamente preciso para distancias medias y largas, alta profundidad de penetración y deformación controlada. Logra agrupaciones pequeñas gracias a su diseño aerodinámico."
  ,descripcionENG: "High quality pellets, correct finish, due to its stamping manufacturing process. Depending on each model, they can be used for hunting, fun and training.<br/>Heavy hunting pellet, extraordinarily accurate for medium and long distances, high depth of penetration and controlled deformation. Achieve small groupings thanks to its aerodynamic design."
  ,id: "21"
  ,nombre: "Puncher"
  ,peso: "2.4 gr – 37 grains"
  ,cb: "0,029"
  },
  {calibre: ".22 - 5.5"
  ,categoria: "PREMIUM"
  ,descripcion: "Balín de caza pesado y preciso para distancias medias.<br/>Alto poder de perforación y profundidad con máximo efecto de choque."
  ,descripcionENG: "Heavy, accurate hunting pellet for medium ranges.<br/>High impact and penetration with maximum shock effect."
  ,id: "22"
  ,nombre: "Hunter"
  ,peso: "1.2 gr – 18,5 grains"
  ,cb: "0,018"
  }];
  
  function abrirModal(imagen){
      let product=productArray.find(a=>a.id==imagen.id);
      document.getElementById("tituloImagen").innerHTML=product.categoria+" - "+product.nombre;
      document.getElementById("imagenMostrar").innerHTML='<img src="'+imagen.src+'" srcset="'+imagen.srcset+'"style="max-height:390px;" alt="pellet">';
      document.getElementById("calibreImagen").innerHTML=product.calibre;
      document.getElementById("pesoImagen").innerHTML=product.peso;
      cbContenedor = document.getElementById("cbContainer");
      if(product.cb === "0"){
        cbContenedor.classList.add("hiddenContainer")
      }else{
        cbContenedor.classList.remove("hiddenContainer");
        document.getElementById("cbImagen").innerHTML=product.cb;
      }
      if(lang=="en"){
       document.getElementById("descripcionImagen").innerHTML=product.descripcionENG;
      }else{
       document.getElementById("descripcionImagen").innerHTML=product.descripcion;
      }
       $("#exampleModal").modal('show');
}