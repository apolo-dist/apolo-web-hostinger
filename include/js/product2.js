var xmlhttp = new XMLHttpRequest();
var descripciones=[];
var calibres=[];
var imprimir=[];
var url = "../content/data/productos-new.json";
var productArray = [];
var mostrarArray = [];
var productItem = "";
var selectedCategoryArray = [];
var showingProducts = 0;
var lang = getUrlVars()["lang"]; 
var totalProducts = 0
var cat;
var calibre=null;
var selectedCategory=null;
var palabra=null;

if (lang == null) {
  lang = "es";
}

function cargarNuevoJson(){
  for (let index = 0; index < descripciones.length; index++) {
    const element = descripciones[index];
    if(element.id){
      let el=productArray.find(a=>(a.id==element.id));
      el.descripcion=element.Descripcion;
      el.presentacion=element.Presentacion;
      el.calibre=element.Calibre;
      el.peso=element.peso;
      imprimir.push(el);
      if(!calibres.find(a=>a==el.calibre)){
        calibres.push(e)
      }
    }
  }
  localStorage.setItem("imprimir",JSON.stringify(imprimir));
}

xmlhttp.open("GET", url, true);
xmlhttp.send();

window.onload=function(){
  if(lang!="es"){
    var soloEs= document.getElementsByClassName("solo-es");
    for (var i = 0; i < soloEs.length; i++) {
      soloEs[i].style.visibility="hidden";
    }
  }
  var inputBuscar=document.getElementById("inputBuscar");
  var botonBuscar=document.getElementById("botonBuscar");
  var botonLimpiar=document.getElementById("botonLimpiar");
  var selectCategoria=document.getElementById("selectCategoria");
  var selectCalibre=document.getElementById("selectCalibre");
  botonLimpiar.addEventListener("click", function(){
    inputBuscar.value="";
    palabra=null;
    selectedCategory=null;
    selectCategoria.value="0";
    selectCalibre.value="0";
    calibre=null;
    var contenedorPrincipal = document.getElementById("product_list_id");
    contenedorPrincipal.setAttribute("Full", "False");
    contenedorPrincipal.setAttribute("cargando", "False");
    contenedorPrincipal.setAttribute('siguiente', '0');
    contenedorPrincipal.innerHTML = ""
    filterProducts(0, true);
  });
  botonBuscar.addEventListener("click", function(){
    if(inputBuscar.value){
      if(inputBuscar.value.length>0){
        palabra=inputBuscar.value;
        filterProducts(0, true);
      }
    } else {
      palabra = "";
      filterProducts(0, true);
    }
  });
  selectCalibre.addEventListener('change', (event) => {
    if(event.target.value=="0"){
      calibre=null;
    }else{
      calibre=event.target.value;
    }
    filterProducts(0, true);
});
  selectCategoria.addEventListener('change', (event) => {
    if(event.target.value=="0"){
      selectedCategory=null;
    }else{
      selectedCategory=event.target.value;
    }
    filterProducts(0, true);
});
}

xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    productArray = JSON.parse(this.responseText);
    filterProducts(0, true);
  } 
};

document.onreadystatechange = function() {
  if (this.readyState == "complete") {
    var category = getUrlVars()["category"];
    if (category != undefined) {
      selectedCategory=category;      
      document.getElementById("selectCategoria").value=selectedCategory;    
      filterProducts(0, true);
    }
  }
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

// what a logic!!
function filterProducts(count, bandera){
  productItem = "";
  showingProducts = 0;
  carga = 12;
  ii = parseInt(count)
  var contenedorPrincipal = document.getElementById("product_list_id");
  if(bandera){
    mostrarArray = [];
    contenedorPrincipal.innerHTML = ""
    contenedorPrincipal.setAttribute("Full", "False");
    for (let i = 0; i < productArray.length; i++) {
      var cond1=true;
      var cond2=true;
      var cond3=true;
      if(calibre){      
        calibre=calibre.replace(/\s+/g, '').toLowerCase().trim();       
        cond1=(calibre==(productArray[i].calibre.replace(/\s+/g, '').toLowerCase().trim()));
      }
      if(selectedCategory){
        cat=selectedCategory.replace(/\s+/g, '').toLowerCase().trim();
        var catProducto=productArray[i].categoria.replace(/\s+/g, '').toLowerCase().trim();
        cond2=catProducto == cat;
      }
      if(palabra){
        palabra=palabra.replace(/\s+/g, '').toLowerCase().trim();
        var buscar=productArray[i].nombre+productArray[i].categoria+productArray[i].descripcion+productArray[i].descripcionENG+productArray[i].presentacion+productArray[i].presentacionENG;
        var nombre=buscar.replace(/\s+/g, '').toLowerCase().trim();
        cond3=nombre.includes(palabra);
      }
      if(cond1 && cond2 && cond3) {
        mostrarArray.push(productArray[i])
      }
    }
  }
  if (contenedorPrincipal.getAttribute("Full") == "False"){
    if (contenedorPrincipal.getAttribute("cargando") == "False"){
      contenedorPrincipal.setAttribute("cargando", "True");
      if (ii+carga > mostrarArray.length) {
        contenedorPrincipal.setAttribute("Full", "True");
      }else{
        contenedorPrincipal.setAttribute('siguiente', ii+carga);
      }
      var i2 = ii;
      for (ii; ii < i2+carga && ii < mostrarArray.length; ii++) {     
        productItem='<div class="col-12 col-md-4 col-lg-3"  id="producto'+mostrarArray[ii].id+'" >'+
        ' <div class=" wrapper"><div class="card inner" style="">'+
        ''+
        '<div class="card-body"><img id="'+mostrarArray[ii].id+'" src="'+mostrarArray[ii].nombreArchivo+'"  class="img-fluid" alt="..." onclick="abrirModal(this)" style="cursor: pointer;"> '+        
        ' </div>'+
        '</div>'+
        '</div></div>';
        showingProducts++;
        contenedorPrincipal.innerHTML += productItem;
      }
      contenedorPrincipal.setAttribute("cargando", "False");
    }
  }
}

function openProductDetail(productId) {
  window.open("../es/product-detail.php?id='" + productId + "'","mywindow");
}

window.onhashchange = function() {
    var actualPage = window.location.href;
    actualPage = actualPage.split("#");
    //alert(actualPage[1]);
}

function updateSelectedCategoryIndex(selectedCategory) {
  selectedCategoryArray.push(selectedCategory)
  for (let i = 0; i<selectedCategoryArray.length; i++) {
    var actualElement = document.getElementById(selectedCategoryArray[i]);
    if (actualElement.classList.contains("current-category")) {
      actualElement.classList.remove("current-category");
    }
  }
  document.getElementById(selectedCategory).classList.add("current-category");
}

function abrirModal(imagen){
  let product=productArray.find(a=>a.id==imagen.id);
  document.getElementById("calibreImagen").innerHTML=product.calibre;
  document.getElementById("pesoImagen").innerHTML=product.peso;
  if(lang=="en"){
   document.getElementById("descripcionImagen").innerHTML=product.descripcionENG;
   document.getElementById("presentacionImagen").innerHTML=product.presentacionENG;
  }else{
   document.getElementById("descripcionImagen").innerHTML=product.descripcion;
   document.getElementById("presentacionImagen").innerHTML=product.presentacion;
  } 
  cbContenedor = document.getElementById("cbContainer");
  if(product.cb === "0"){
    cbContenedor.classList.add("hiddenContainer")
  }else{
    cbContenedor.classList.remove("hiddenContainer");
    document.getElementById("cbImagen").innerHTML=product.cb;
  }
   document.getElementById("tituloImagen").innerHTML=product.categoria;
   document.getElementById("imagenMostrar").innerHTML='<img src="'+imagen.src+'" style="max-height:500px;" alt="">';
   $("#exampleModal").modal('show');
 }