function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
var lang = getUrlVars()["lang"];
    
function abrirAnuncio2(){
    if(lang=="en"){
        document.getElementById("tituloAdv2").innerHTML="Warning";
        document.getElementById("advertencia2").innerHTML="Sales only for Argentina";
        document.getElementById("ok2").innerHTML="Continue";
        document.getElementById("cancel2").innerHTML="Cancel";
    }else{
        document.getElementById("tituloAdv2").innerHTML="Advertencia";
        document.getElementById("advertencia2").innerHTML="Ventas solo para Argentina";
        document.getElementById("ok2").innerHTML="Continuar";
        document.getElementById("cancel2").innerHTML="Cancelar";
    }
    $("#advertenciaModal2").modal('show');
}