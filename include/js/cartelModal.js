function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
var lang = getUrlVars()["lang"];
    
function abrirAnuncio(){
    if(lang=="en"){
        document.getElementById("tituloAdv").innerHTML="Warning";
        document.getElementById("advertencia").innerHTML="Sales only for Argentina";
        document.getElementById("ok").innerHTML="Continue";
        document.getElementById("cancel").innerHTML="Cancel";
    }else{
        document.getElementById("tituloAdv").innerHTML="Advertencia";
        document.getElementById("advertencia").innerHTML="Ventas solo para Argentina";
        document.getElementById("ok").innerHTML="Continuar";
        document.getElementById("cancel").innerHTML="Cancelar";
    }
    $("#advertenciaModal").modal('show');
}