var xmlhttp = new XMLHttpRequest();
var url = "../../content/data/empresa.json";
var empresa={};

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
      empresa = JSON.parse(this.responseText);
  }
};

function cargarAniosTrayectoria(){
  document.getElementById('aniosTrayectoria').innerText=new Date().getFullYear()-2000;
}
/*
function cargarCalesita(){
  var fotosString="";
  for (let index = 0; index < empresa.fotos.length; index++) {
    const element = empresa.fotos[index];
    if(index==0){
      fotosString=fotosString+'<div class="carousel-item active"><img class="d-block w-100" src="../../content/image/empresa/'+element+'" ></div>'
    }else{
      fotosString=fotosString+'<div class="carousel-item "><img class="d-block w-100" src="../../content/image/empresa/'+element+'" ></div>'
    }
  }   
  document.getElementById("calesita").innerHTML=fotosString;
}
*/
window.onload=function(){
  getUrlVars()
  //cargarCalesita()
  cargarAniosTrayectoria();
}

  