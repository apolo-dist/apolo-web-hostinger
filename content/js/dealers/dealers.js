function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

window.onload=function(){
    var map;
    var dataMap = {};
    //var colorSelect = "#876B20";
    var colorSelect = "#C9BB64"; 


    window.onclick = function(event) {
        
      }
    
    document.getElementById("botonMundo").onclick = function () {
        var zoomOpts0 = {
            scaleFactor: 1, // The amount to zoom
            center: {
                lat: 0, // latitude of the point to which you wish to zoom
                lng: 0, // longitude of the point to which you wish to zoom

            },
            transition: {
                duration: 1000
            },
            onZoomComplete: function (zoomData) {

            }
        };
        map.zoomto(zoomOpts0)
    }
   
   
    fetch('../content/data/paises.json')
        .then(res => res.json())
        .then((out) => {
    
            dataMap = {};
            for (var index = 0; index < out.length; index++) {
                var element = out[index];
                dataMap[out[index].COD] = {
                    fillKey: out[index].CONTINENTE,
                    nombre: out[index].NOMBRE,
                    lat: out[index].LAT,
                    lng: out[index].LNG
                }
            }
    
            map = new Datamap({
                element: document.getElementById('mapa'),
    
                fills: {                  
                    AMERICA: colorSelect,
                    AFRICA: colorSelect,
                    EUROPA: colorSelect,
                    ASIA: colorSelect,
                    OCEANIA: colorSelect,
                    UNKNOWN: colorSelect,
                    defaultFill: 'BLACK',
                },
                data: dataMap,
                geographyConfig: {
                    popupTemplate: function (geo, data) {
                        var paiss = map.options.data[geo.id]
                        var lang = getUrlVars()["lang"];
                        if(paiss.nombre=="ARGENTINA"){
                            if(lang=="en"){
                                return ['<div class="hoverinfo"><strong>',
                                'List of distributors of ' + data.nombre +
                                '</strong></div>'
                            ].join('');
                            }else{
                                return ['<div class="hoverinfo"><strong>',
                                'Lista de distribuidores de ' + data.nombre +
                                '</strong></div>'
                            ].join('');
                            }
                        } else {
                            if(lang=="en"){
                                return ['<div class="hoverinfo"><strong>', 'For more information on ' + data.nombre + ', request info@apolo.ar' + '</strong></div>'].join('');
                            }else{
                                return ['<div class="hoverinfo"><strong>', 'Para más información sobre  ' + data.nombre + ', solicitar a info@apolo.ar' + '</strong></div>'].join('');
                            }
                        }
                    },
                    highlightFillColor: function (data) {
                        if (data.fillKey) {
                            /* color al poner el puntero encima */
                            return '#fac328';
                        }
                        return '#000000';
                    },
                },
                done: function (map) {
                    window.onload = function () {
                        // code to execute here
                        
                    };
                    map.svg.selectAll('.datamaps-subunit').on('click', function (geo, $evt) {
                        var localData = map.options.data[geo.id]
                        if(localData.nombre=="ARGENTINA"){
                            $('#myModal').modal('show');
                        }
                        
                        if (localData) {
                            zoomOpts.center.lat = localData.lat;
                            zoomOpts.center.lng = localData.lng;
                            map.zoomto(zoomOpts)
                            //alert(localData)
                        }
                    })
                }
            });
            var zoomOpts = {
                scaleFactor: 2, // The amount to zoom
                center: {
                    lat: 0, // latitude of the point to which you wish to zoom
                    lng: 0, // longitude of the point to which you wish to zoom
                },
                transition: {
                    duration: 1000 // milliseconds
                },
                onZoomComplete: function (zoomData) {
    
                }
            };
    
        }).catch(err => console.error(err));
}
